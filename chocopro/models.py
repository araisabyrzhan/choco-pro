from django.db import models
from django.contrib.auth.models import User

from .signals import save_comment
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _


class Blog(models.Model):
    blog_name = models.CharField(max_length=128, verbose_name=_("blog_name"))
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_("created"))
    updates = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name=_("updated"))

    def __str__(self):
        return "%s" % self.blog_name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Post(models.Model):
    title = models.CharField(max_length=200, verbose_name=_("title"))
    slug = models.SlugField()
    content = models.TextField(verbose_name=_("Post content"))
    image = models.ImageField(upload_to='static/media/postimg/', blank=True, null=True, default='static/media/postimg/post.jpg', verbose_name=_("Post image"))
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("author"))
    blog = models.ForeignKey(Blog,verbose_name=_("blog"))
    rating = models.IntegerField(default=0, verbose_name=_("count rating"))
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_("created"))
    updates = models.DateTimeField(auto_now_add=False, auto_now=True, verbose_name=_("updated"))
    allow_post = models.BooleanField(default=False, verbose_name=_("allow post"))
    allow_comments = models.BooleanField(default=True,verbose_name=_("allow comments"))

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"
        ordering = ['created']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug,
        }

        return reverse('post_view', kwargs=kwargs)


class Comment(models.Model):
    comPost = models.ForeignKey(Post, related_name='comments',verbose_name=_("post"))        # Название статьи/поста
    comAuthor = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comment_author', verbose_name=_("author"))   # ID Автора
    comContent = models.TextField(verbose_name=_("message"))  # Комментарий
    comCreated = models.DateTimeField(auto_now_add=True,verbose_name=_("created"))  # Дата публикации

    def __str__(self):
        return "%s - %s" % (self.id, self.comContent)

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = _("Comments")
        ordering = ['comCreated']


#post_save.connect(save_comment, sender=Comment)