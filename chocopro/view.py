from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import login, authenticate
from django.contrib import auth

from django.conf import settings
from .models import Blog, Post, Comment
from .forms import *


top_five_post = Post.objects.order_by('-rating')[:5]    # Топ 5 лучших постов
latest_post = Post.objects.order_by('-created')[:3]     # Недавно опубликованные
blogs = Blog.objects.all().select_related()[:5]         # 5 категории

lists = {'top_post': top_five_post, 'blogs': blogs, 'lpost': latest_post}

def checkin(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/login/')
    else:
        form = SignUpForm()
    return render(request, 'auth/checkin.html', {'form': form})


def profile(request):
    user = User.objects.get(username=auth.get_user(request).username)
    return render(request, "auth/profile.html", {'user': user, 'l': lists, 'username': auth.get_user(request).username})


def own_post(request):
    user = User.objects.get(username=auth.get_user(request).username)
    posts = Post.objects.filter(author=user)
    return render(request, 'post.html', {'posts': posts, 'l': lists, 'username': auth.get_user(request).username})


def login(request, user):
    args = {}
    args['form'] = UserCreationForm()
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request,user)
            return redirect('/')
        else:
            args['login_error'] = "Пользователь не найден"
            return render_to_response('auth/login.html',args)
    else:
        return render_to_response('auth/login.html',args)


def logout(request):
    auth.logout(request)
    return redirect("/")


def authors(request):
    author_list = User.objects.all().select_related()
    return render(request, 'authors.html', {'author': author_list, 'l': lists, 'username': auth.get_user(request).username })


def authors_post(request, username_post):
    user = User.objects.get(username=username_post)
    posts = Post.objects.filter(author=user)
    return render(request, 'post.html', {'posts': posts, 'l': lists, 'username': auth.get_user(request).username})


def blog(request):
    blog_list = Blog.objects.all().select_related()
    return render(request, 'blog.html', {'blog': blog_list, 'l': lists, 'username': auth.get_user(request).username})


def blog_view(request, blog_id):
    posts = Post.objects.filter(blog=blog_id)
    return render(request, 'post.html', {'posts': posts, 'l': lists, 'username': auth.get_user(request).username})


def top(request):
    posts = Post.objects.order_by('-rating')
    return render(request, 'post.html', {'posts': posts, 'l': lists, 'username': auth.get_user(request).username})


def post(request):
    posts = Post.objects.all().select_related()
    page = request.GET.get('page', 1)
    paginator = Paginator(posts, 2)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request, 'post.html', {'posts': posts, 'l': lists, 'username': auth.get_user(request).username})


def post_view(request, slug):
    postv = Post.objects.get(slug=slug)
    comnt = Comment.objects.filter(comPost=postv)
    return render(request, 'post_view.html', {'postv': postv, 'comnt': comnt, 'l': lists,
                                              'username': auth.get_user(request).username})


def add_post(request):
    author = User.objects.get(username=auth.get_user(request).username)
    if request.method == 'POST':
        form = AddPost(request.POST)
        if form.is_valid():

            title = form.cleaned_data.get('title', '')
            slug = form.cleaned_data.get('slug', '')
            content = form.cleaned_data.get('content', '')
            image = form.cleaned_data.get('image', '')
            blog = form.cleaned_data.get('blog', '')
            allow_post = form.cleaned_data.get('allow_post', '')

            # post = authenticate(title=title, slug=slug, content=content, image=image,blog=blog, allow_post=allow_post, author_id=author.id)

            newPosts = Post(title=title, slug=slug, content=content, image=image,
                                blog=blog, allow_post=allow_post, author_id=author.id)
            newPosts.save()

            args = "Ваш пост успешно опубликован!"
        else:
            args = "Ошибка"

        return render_to_response('add_post.html',  {'form': form, 'args': args})
    else:
        form = AddPost()
        return render(request, 'add_post.html', {'form': form, 'username': auth.get_user(request).username, 'id': author.id})





