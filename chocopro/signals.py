def save_comment(sender, instance, created, **kwargs):
    if created:
        comment = instance
        post = comment.post
        post.save(force_update=True)