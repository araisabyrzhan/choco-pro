from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.forms import ModelForm
from .models import Post


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password'}))


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False)
    last_name = forms.CharField(max_length=30, required=False)
    your_photo = forms.ImageField(required=False)   # upload_to='static/media/postimg/',

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', 'your_photo')


class AddPost(ModelForm):
    title = forms.CharField(max_length=250, required=False)
    slug = forms.CharField(max_length=30, required=False)
    content = forms.Textarea()
    image = forms.ImageField(required=False)
    blog = forms.SelectMultiple()
    allow_post = forms.SelectMultiple()

    class Meta:
        model = Post
        fields = ('title', 'slug', 'content', 'image', 'blog', 'allow_post')