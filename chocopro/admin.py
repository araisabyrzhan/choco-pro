from django.contrib import admin
from .models import *


class BlogAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Blog._meta.fields]

    class Meta:
        model = Blog


admin.site.register(Blog, BlogAdmin)


class PostAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Post._meta.fields]

    class Meta:
        model = Post


admin.site.register(Post, PostAdmin)


class CommentAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Comment._meta.fields]

    class Meta:
        model = Comment


admin.site.register(Comment, CommentAdmin)