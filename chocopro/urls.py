"""chocopro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from .view import *
from .forms import LoginForm
from chocopro import view

admin.autodiscover()

urlpatterns = (
    url(r'^admin/$', admin.site.urls),
    url(r'^$', view.post, name='post'),
    url(r'^post/$', view.post, name='post'),
    url(r'^post/(?P<slug>[-_\w]+)/$', view.post_view, name='post_view'),
    url(r'^add_post/$', view.add_post, name='add_post'),

    url(r'^auth/checkin/$', view.checkin, name='checkin'),
    url(r'^login/$', auth_views.login, {'template_name': 'auth/login.html', 'authentication_form': LoginForm},
        name='login'),
    url(r'^accounts/profile/$', view.profile, name='profile'),
    url(r'^accounts/own_post/$', view.own_post, name='own_post'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login'}, name='logout'),

    url(r'^top/$', view.top, name='top'),
    url(r'^blog/$', view.blog, name='blog'),
    url(r'^blog/(?P<blog_id>[-_\w]+)/$', view.blog_view, name='blog_view'),
    url(r'^authors/$', view.authors, name='authors'),
    url(r'^authors/(?P<username_post>[-_\w]+)/$', view.authors_post, name='authors_post'),
)
